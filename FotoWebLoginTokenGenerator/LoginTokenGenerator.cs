// LoginTokenGenerator.cs - Example code for generating FotoWeb login tokens
// (C) 1995-2018 FotoWare a.s.
// See LICENSE.txt for terms of use and distribution

using System;
using System.IO;
using System.Security.Cryptography;

namespace FotoWareNet.FotoWeb
{
    /// <summary>
    /// Helper class for creating and parsing login tokens.
    /// Usage:
    /// 
    /// LoginTokenGenerator g = new LoginTokenGenerator(encryptionSecret);
    /// 
    /// string token = g.CreateLoginToken(userName);
    /// 
    /// if(!g.DecodeLoginToken(token, userName, remainingTime)) {
    ///   // invalid token
    /// }
    /// 
    /// Format of a login token:
    /// s=START_TIME;e=END_TIME;w=0|1;u=USERNAME
    /// where:
    /// - START_TIME: Start time of validity of the token in "yyyy-MM-dd HH:mm:ss" format
    /// - END_TIME: End time of validity of the token in "yyyy-MM-dd HH:mm:ss" format
    /// - USERNAME: FotoWeb username of the user. Must be percent-encoded (URL-encoded)
    /// - Set w=0 foir regular login tokens and w=1 for tokens to be used with FotoWeb widgets.
    /// 
    /// For more information, see:
    /// - http://learn.fotoware.com/02_FotoWeb_8.0/Developing_with_the_FotoWeb_API/Login_Tokens
    /// - http://learn.fotoware.com/02_FotoWeb_8.0/Developing_with_the_FotoWeb_API/User_Authentication_for_Embeddable_Widgets
    /// 
    /// LEGACY NOTE:
    /// - Login tokens for widgets are a legacy SSO feature. Embddable widgets now have full support for FotoWeb SSO.
    /// - Login tokens are still necessary and useful if the user is authenticated by an external application that is not an SSO provider for FotoWeb.
    /// - If user authentication is done using Active Directory or Azure AD, you can use FotoWeb SSO instead using SAML+ADFS or Azure AD integration
    /// Please see the documentation for more details.
    /// </summary>
    public class LoginTokenGenerator
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="encryptionSecret">Encryption secret of the site or tenant</param>
        public LoginTokenGenerator(string encryptionSecret)
        {
            EncryptionSecret = encryptionSecret;
        }

        /// <summary>
        /// Time for which login tokens are valid
        /// </summary>
        public TimeSpan ValidityPeriod { get; set; } = TimeSpan.FromMinutes(5);

        /// <summary>
        /// Login token is used for embeddable widgets
        /// </summary>
        public bool UseForWidgets { get; set; } = false;

        /// <summary>
        /// Encryption secret of the site or tenant
        /// </summary>
        public string EncryptionSecret { get; set; }

        /// <summary>
        /// Margin for clock skew between issuer and consumer of the login token.
        /// If the token is created on the same machine FotoWeb runs on, this can be zero (default).
        /// Set to a higher value if you get "Token is not valid yet" errors.
        /// </summary>
        public TimeSpan ClockSkewMargin { get; set; } = TimeSpan.Zero;

        /// <summary>
        /// Create a login token.
        /// </summary>
        /// <param name="username">Username of user to create login token for</param>
        /// <returns>String representation of login token</returns>
        public string CreateLoginToken(string username)
        {
            DateTime startTime = DateTime.UtcNow - ClockSkewMargin;
            DateTime endTime = startTime + ValidityPeriod + ClockSkewMargin;

            string token = String.Format("s={0};e={1};w={2};u={3};", _ToUniversalDateTimeString(startTime), _ToUniversalDateTimeString(endTime), UseForWidgets, username);
            token += "m=" + _CreateStringMac(token) + ";";

            byte[] utf8String = _StringAsUtf8ByteArray(token);
            return Convert.ToBase64String(utf8String);
        }

        /// <summary>
        /// Decode a login token.
        /// This is normally done by FotoWeb. Used for testing only.
        /// </summary>
        /// <param name="token">String representation of login token</param>
        /// <param name="userName">Username of user to create login token for</param>
        /// <param name="remainingTime">Remaining Validity time from now</param>
        /// <returns></returns>
        public bool DecodeLoginToken(string token, out string userName, out TimeSpan remainingTime)
        {
            userName = null;
            remainingTime = TimeSpan.FromSeconds(0);

            string tokenData = _Utf8ByteArrayToString(Convert.FromBase64String(token));
            string data = tokenData.Substring(0, tokenData.LastIndexOf(";m=") + 1);
            string mac = tokenData.Substring(tokenData.LastIndexOf(";m=") + 3);
            mac = mac.Substring(0, mac.Length - 1);

            if (mac != _CreateStringMac(data))
                return false;

            userName = _GetParamFromTokenData(data, "u");
            DateTime startTime = _ParseUniversalDateTimeString(_GetParamFromTokenData(data, "s"));
            DateTime endTime = _ParseUniversalDateTimeString(_GetParamFromTokenData(data, "e"));

            if (DateTime.UtcNow < startTime)
                return false;

            if (DateTime.UtcNow > endTime)
                return false;

            remainingTime = endTime - DateTime.UtcNow;
            return true;
        }

        private static string _ToUniversalDateTimeString(DateTime date)
        {
            return (date.ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture));
        }

        private static DateTime _ParseUniversalDateTimeString(string dateString)
        {
            return (DateTime.ParseExact(dateString, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture));
        }

        private static string _GetParamFromTokenData(string data, string prmName)
        {
            prmName = prmName + "=";
            int startPos = data.IndexOf(prmName) + prmName.Length;
            int endPos = data.IndexOf(";", startPos);

            return data.Substring(startPos, endPos - startPos);
        }

        private string _CreateStringMac(string data)
        {
            data = data + "es=" + EncryptionSecret;
            byte[] dataAsUtf8 = _StringAsUtf8ByteArray(data);

            MD5 hasher = new MD5CryptoServiceProvider();
            byte[] mac = hasher.ComputeHash(dataAsUtf8);

            return Convert.ToBase64String(mac);
        }

        private static byte[] _StringAsUtf8ByteArray(string data)
        {
            MemoryStream ms = new MemoryStream(100);
            StreamWriter sw = new StreamWriter(ms, new System.Text.UTF8Encoding(false, false));
            sw.Write(data);
            sw.Flush();

            return ms.ToArray();
        }

        private static string _Utf8ByteArrayToString(byte[] data)
        {
            MemoryStream ms = new MemoryStream(data);
            StreamReader sr = new StreamReader(ms, System.Text.Encoding.UTF8);
            return sr.ReadToEnd();
        }
    }
}
