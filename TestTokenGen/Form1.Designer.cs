﻿namespace TestTokenGen
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.txtVal = new System.Windows.Forms.TextBox();
            this.txtUsrName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGenToken = new System.Windows.Forms.Button();
            this.txtTokenOut = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnDecodeToken = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDecUsrName = new System.Windows.Forms.TextBox();
            this.txtRemHours = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEncSecret = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.useForWidgets = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(80, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Validity in minutes";
            // 
            // txtVal
            // 
            this.txtVal.Location = new System.Drawing.Point(192, 16);
            this.txtVal.Name = "txtVal";
            this.txtVal.Size = new System.Drawing.Size(32, 20);
            this.txtVal.TabIndex = 1;
            this.txtVal.Text = "1";
            // 
            // txtUsrName
            // 
            this.txtUsrName.Location = new System.Drawing.Point(192, 80);
            this.txtUsrName.Name = "txtUsrName";
            this.txtUsrName.Size = new System.Drawing.Size(100, 20);
            this.txtUsrName.TabIndex = 2;
            this.txtUsrName.Text = "chrisf";
            this.txtUsrName.TextChanged += new System.EventHandler(this.txtUsrName_TextChanged);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(80, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 3;
            this.label2.Text = "User name";
            // 
            // btnGenToken
            // 
            this.btnGenToken.Location = new System.Drawing.Point(83, 145);
            this.btnGenToken.Name = "btnGenToken";
            this.btnGenToken.Size = new System.Drawing.Size(75, 23);
            this.btnGenToken.TabIndex = 4;
            this.btnGenToken.Text = "&Generate";
            this.btnGenToken.Click += new System.EventHandler(this.btnGenToken_Click);
            // 
            // txtTokenOut
            // 
            this.txtTokenOut.Location = new System.Drawing.Point(83, 201);
            this.txtTokenOut.Name = "txtTokenOut";
            this.txtTokenOut.Size = new System.Drawing.Size(216, 20);
            this.txtTokenOut.TabIndex = 5;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(16, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // btnDecodeToken
            // 
            this.btnDecodeToken.Location = new System.Drawing.Point(83, 241);
            this.btnDecodeToken.Name = "btnDecodeToken";
            this.btnDecodeToken.Size = new System.Drawing.Size(75, 23);
            this.btnDecodeToken.TabIndex = 7;
            this.btnDecodeToken.Text = "&Decode";
            this.btnDecodeToken.Click += new System.EventHandler(this.btnDecodeToken_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(83, 281);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 24);
            this.label3.TabIndex = 8;
            this.label3.Text = "User Name";
            // 
            // txtDecUsrName
            // 
            this.txtDecUsrName.Location = new System.Drawing.Point(195, 281);
            this.txtDecUsrName.Name = "txtDecUsrName";
            this.txtDecUsrName.Size = new System.Drawing.Size(100, 20);
            this.txtDecUsrName.TabIndex = 9;
            // 
            // txtRemHours
            // 
            this.txtRemHours.Location = new System.Drawing.Point(195, 313);
            this.txtRemHours.Name = "txtRemHours";
            this.txtRemHours.Size = new System.Drawing.Size(100, 20);
            this.txtRemHours.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(83, 313);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 24);
            this.label4.TabIndex = 8;
            this.label4.Text = "Rem. minutes";
            // 
            // txtEncSecret
            // 
            this.txtEncSecret.Location = new System.Drawing.Point(192, 48);
            this.txtEncSecret.Name = "txtEncSecret";
            this.txtEncSecret.Size = new System.Drawing.Size(100, 20);
            this.txtEncSecret.TabIndex = 2;
            this.txtEncSecret.Text = "0123456789";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(80, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 23);
            this.label5.TabIndex = 3;
            this.label5.Text = "Encryption secret";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(83, 185);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "Token:";
            // 
            // useForWidgets
            // 
            this.useForWidgets.AutoSize = true;
            this.useForWidgets.Location = new System.Drawing.Point(86, 113);
            this.useForWidgets.Name = "useForWidgets";
            this.useForWidgets.Size = new System.Drawing.Size(99, 17);
            this.useForWidgets.TabIndex = 13;
            this.useForWidgets.Text = "Use for widgets";
            this.useForWidgets.UseVisualStyleBackColor = true;
            this.useForWidgets.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(328, 345);
            this.Controls.Add(this.useForWidgets);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtRemHours);
            this.Controls.Add(this.txtDecUsrName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnDecodeToken);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtTokenOut);
            this.Controls.Add(this.txtUsrName);
            this.Controls.Add(this.txtVal);
            this.Controls.Add(this.btnGenToken);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtEncSecret);
            this.Controls.Add(this.label5);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Login Token Generator";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}

