﻿// TestTokenGen.cs - Implementation of GUI tool for login token generation
// (C) 1995-2015 FotoWare a.s.
// See LICENSE.txt for terms of use and distribution

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestTokenGen
{
    static class TestTokenGen
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
