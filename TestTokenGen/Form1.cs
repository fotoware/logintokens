// Form1.cs - Implementation of GUI tool for login token generation
// (C) 1995-2015 FotoWare a.s.
// See LICENSE.txt for terms of use and distribution

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace TestTokenGen
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public partial class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtUsrName;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnGenToken;
		private System.Windows.Forms.TextBox txtTokenOut;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button btnDecodeToken;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtDecUsrName;
		private System.Windows.Forms.TextBox txtRemHours;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtVal;
		private System.Windows.Forms.TextBox txtEncSecret;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
        private CheckBox useForWidgets;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		private void btnGenToken_Click(object sender, System.EventArgs e)
		{
			FotoWareNet.FotoWeb.LoginTokenGenerator ltg = new FotoWareNet.FotoWeb.LoginTokenGenerator( txtEncSecret.Text );
			ltg.ValidityPeriod = TimeSpan.FromMinutes( Int32.Parse( txtVal.Text ) );
            ltg.UseForWidgets = useForWidgets.Checked;
			txtTokenOut.Text = ltg.CreateLoginToken( txtUsrName.Text );
			Clipboard.SetDataObject( System.Web.HttpUtility.UrlEncode( System.Web.HttpUtility.UrlEncode( txtTokenOut.Text ) ), true );
		}

		void TestTokenGeneration()
		{
			FotoWareNet.FotoWeb.LoginTokenGenerator ltg = new FotoWareNet.FotoWeb.LoginTokenGenerator( "encryptionSecretString" );
			ltg.ValidityPeriod = TimeSpan.FromMinutes( 30 );
			string token = ltg.CreateLoginToken( txtUsrName.Text );

			string urlParamToAppendToQueryString = "&lt=" + System.Web.HttpUtility.UrlEncode( token );
		}

		private void btnDecodeToken_Click(object sender, System.EventArgs e)
		{
			FotoWareNet.FotoWeb.LoginTokenGenerator ltg = new FotoWareNet.FotoWeb.LoginTokenGenerator( txtEncSecret.Text );

			string usr;
			TimeSpan remTime;
			if ( ltg.DecodeLoginToken( txtTokenOut.Text, out usr, out remTime ) )
			{
				txtDecUsrName.Text = usr;
				txtRemHours.Text = remTime.TotalMinutes.ToString();	
			}
			else
			{
				txtDecUsrName.Text = "<INVALID TOKEN>";
			}
		}

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void txtUsrName_TextChanged(object sender, EventArgs e)
        {

        }
	}
}
